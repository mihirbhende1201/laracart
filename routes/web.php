<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'CartController@home')->name('home');
Route::get('/home', 'CartController@home')->name('home');
Route::get('/', 'CartController@home')->name('home');
Route::get('/cart', 'CartController@index')->name('cart');
Route::get('/add/{name}/{price}/{sku}', 'CartController@add')->name('add');
Route::get('/remove/{rowId}', 'CartController@destroy')->name('remove');
