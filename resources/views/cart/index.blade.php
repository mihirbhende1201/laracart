@extends('layouts.app')
	@section('content')
        <div class="container">
        	@if(Cart::count() > 0)
				<table class="table table-bordered">
				    <thead>
				        <tr>
				            <th>Product</th>
				            <th width="100">Qty</th>
				            <th>Row Id : Internal</th>
				            <th>Price</th>
				            <th>Subtotal</th>
				            <th>Remove</th>
				        </tr>
				    </thead>

				    <tbody>

				        <?php foreach(Cart::content() as $row) :?>

				            <tr>
				                <td>
				                    <p><strong><?php echo $row->name; ?></strong></p>
				                    <p><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></p>
				                </td>
				                <td><input type="text" class="form-control" value="<?php echo $row->qty; ?>"></td>
				                <td>$<?php echo $row->rowId; ?></td>
				                <td>$<?php echo $row->price; ?></td>
				                <td>$<?php echo $row->total; ?></td>
				                <td><a href="{{ route('remove',['rowId' => $row->rowId ]) }}">X</a></td>
				            </tr>

				        <?php endforeach;?>

				    </tbody>

				    <tfoot>
				        <tr>
				            <td colspan="3">&nbsp;</td>
				            <td>Subtotal</td>
				            <td><?php echo Cart::subtotal(); ?></td>
				        </tr>
				        <tr>
				            <td colspan="3">&nbsp;</td>
				            <td>Tax</td>
				            <td><?php echo Cart::tax(); ?></td>
				        </tr>
				        <tr>
				            <td colspan="3">&nbsp;</td>
				            <td>Total</td>
				            <td><?php echo Cart::total(); ?></td>
				        </tr>
				    </tfoot>
				</table>
				<p class="text-center"><a href="{{ route('home') }}" class="btn btn-default">Continue Shopping</a></p>
			@else
				<p class="text-center">There are no items in cart</p>
				<p class="text-center"><a href="{{ route('home') }}" class="btn btn-default">Start Shopping</a></p>
			@endif
        </div>
    </div>
@endsection