<!DOCTYPE html>
<html lang="en">
@include('includes.head')
<body>
    <div id="app">
        @include('includes.nav')
        @yield('content')
    </div>
    <script src="/js/app.js"></script>
    @yield('scripts')
    @yield('before_body_ends')
</body>
</html>
