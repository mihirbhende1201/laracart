@extends('layouts.app')
@section('content')
    <div class="container">
            <div class="well well-sm text-center">
                <strong class="text-center">Browse Products</strong>
            </div>
    <div id="products" class="row list-group">
        @foreach($products as $item)
            <div class="item  col-xs-4 col-lg-4">
                <div class="thumbnail">
                    <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                    <div class="caption">
                        <h4 class="group inner list-group-item-heading">
                           {{ $item['name'] }}</h4>
                        <p class="group inner list-group-item-text">
                            {{ $item['description'] }}    
                        </p>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p class="lead">{{ $item['price'] }}</p>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <a class="btn btn-success" href="{{ route('add', ['name' => $item['name'], 'price' => $item['price'], 'sku' => $item['sku']]) }}">Add to cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>  
    </div>
@endsection